require('dotenv').config()

const { join } = require('path')
const express = require('express')
const morgan = require('morgan')
const cors = require('cors')
const Discord = require('discord.js')

const app = express()
const client = new Discord.Client()
const channelsWhitelist = JSON.parse(process.env.CHANNELS)
const wordPattern = new RegExp(process.env.WORD_PATTERN, 'gi')
const connsAlive = []
const wordTimeline = {}
function writeFrame (conn, frame) {
  conn.write(`data: ${JSON.stringify(frame)}\n\n`)
}

app.use(cors())
app.use(express.static(join(__dirname, 'dist')))
app.use(morgan('tiny'))

app.get('/', (req, res) => {
  res.sendFile(join(__dirname, 'dist/index.html'))
})
app.get('/events', async (req, res) => {
  console.log('client connected, sending initial frame')
  console.time('initial frame')
  res.set({
    'Content-Type': 'text/event-stream',
    'Cache-Control': 'no-cache',
    Connection: 'keep-alive',
  })
  connsAlive.push(res)
  req.on('close', () => {
    connsAlive.splice(connsAlive.indexOf(res), 1)
  })
  const wordHeapmap = {}
  for (const words of Object.values(wordTimeline)) {
    for (const word of words) {
      wordHeapmap[word] = (wordHeapmap[word] || 0) + 1
    }
  }
  writeFrame(res, { type: 'DATA', words: wordHeapmap })
  writeFrame(res, { type: 'HEARTBEAT' })
  console.timeEnd('initial frame')
})

setInterval(() => {
  for (const conn of connsAlive) {
    writeFrame(conn, { type: 'HEARTBEAT' })
  }
}, 2000)
setInterval(() => {
  const now = Date.now()
  for (const time of Object.keys(wordTimeline)) {
    if (now - time <= 1000 * +process.env.TIMELINE_TTL) break
    delete wordTimeline[time]
  }
}, 1000 * 60 * 5)
app.on('data:frame', frame => {
  for (const conn of connsAlive) {
    writeFrame(conn, frame)
  }
})
client.on('message', async message => {
  if (channelsWhitelist.includes(message.channel.id)) {
    if (!message.author || message.author.bot) return
    const wordMatches = message.cleanContent
      .replace(/'|\u2019/g, '')
      .match(wordPattern)
    if (!wordMatches) return
    const words = wordMatches
      .map(word => word.toLowerCase())
      .filter(word => word.length >= +process.env.WORD_MIN_LEN)
      .filter(word => word.length <= +process.env.WORD_MAX_LEN)
      .filter((word, i, a) => a.indexOf(word) === i)
    if (!words.length) return
    console.log('words added:', words)
    wordTimeline[Date.now()] = words
  }
})

client.login(process.env.TOKEN).then(() => {
  console.log('client connected')
  app.listen(process.env.PORT, () => {
    console.log(`app listening to port ${process.env.PORT}`)
  })
}).catch(console.error)
