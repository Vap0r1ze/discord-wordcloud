import 'normalize.css'
import './style.css'
import WordCloud from 'wordcloud/src/wordcloud2'
import colormap from 'colormap'
// import anime from 'animejs'
const { EventSource, localStorage } = window

const es = new EventSource('/events')
const canvas = document.getElementById('canvas')
const wordBlacklist = 'the that this have and you your for not like its just http https was'.split(' ')
const getWordCloudOptions = (list, min, max) => ({
  list,
  gridSize: Math.round(16 * canvas.width / 1024),
  weightFactor (size) {
    const maxFactor = (document.body.clientHeight > document.body.clientWidth) ? 250 : 100
    const x = (size - min) / (max - min)
    const factor = 50 + (x ** 2 * maxFactor) * canvas.width / 1024
    console.log(factor)
    return factor
  },
  fontFamily: 'Times, serif',
  color (word, weight) {
    const x = (weight - min) / (max - min)
    const color = colormap({
      colormap: 'density',
      nshades: 21,
      format: 'hex',
      alpha: 1,
    })[Math.floor((1 - x) * 15)]
    return color
  },
  rotateRatio: 0.5,
  rotationSteps: 2,
  shuffle: false,
})

es.onmessage = async e => {
  if (localStorage.debug) console.time('parsed frame')
  const data = JSON.parse(e.data)
  if (localStorage.debug) {
    console.timeEnd('parsed frame')
    console.log(`frame type: ${data.type}`)
  }
  switch (data.type) {
    case 'DATA': {
      if (localStorage.debug) console.log('data frame recieved', data)
      const wordsList = []
      for (const [word, heat] of Object.entries(data.words)) {
        if (word.length <= 2) continue
        if (wordBlacklist.includes(word)) continue
        wordsList.push([word, heat])
      }
      const weights = wordsList.map(word => word[1])
      const wordCloudOptions = getWordCloudOptions(wordsList, Math.min(...weights), Math.max(...weights))
      logic(wordCloudOptions)
      break
    }
  }
}
function logic (wordCloudOptions) {
  canvas.width = canvas.clientWidth
  canvas.height = canvas.clientHeight
  WordCloud(canvas, wordCloudOptions)
}
